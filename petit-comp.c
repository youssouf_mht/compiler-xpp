/* fichier: "petit-comp.c" */

/* Un petit compilateur et machine virtuelle pour un sous-ensemble de C.  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>
jmp_buf env;

// definition de la structure grand_entier et cellule
struct cellule {
    int chiffre;
    struct cellule* suivant;
};

struct grand_entier {  // la strucute representant les grand_entiers;
    int negatif;
    struct cellule *chiffres;
};
typedef struct grand_entier grand_entier;
typedef struct cellule cellule;


// les declarations des methodes de la structure cellule

cellule* new_cellule(int c);
int length(cellule* n);
void free_nombre(cellule *m);
void printList(cellule* m);
cellule* make_empty_list(int len);
cellule* soustraire(cellule* premier,cellule* second);
cellule* modulo_10(cellule* premier);
cellule* divise(cellule* n1 , int len);
cellule* reverse_iterative(cellule* head);

// les declarations des methodes de la structure grand_entier

grand_entier* entier_tab[];// le tableau pour liberer les entiers lors de la fin du programme;
int entier_count = 0; // le nombre total d'entier utiliser dans le programme;
int comparer_entier(grand_entier* n1, grand_entier* n2);
grand_entier* completer_sequence(grand_entier* entier,int c);
grand_entier* addition(grand_entier* n1, grand_entier* n2);
grand_entier* multiplication(grand_entier* n1, grand_entier* n2);
grand_entier* modulo_entier(grand_entier* n1 , grand_entier* n2);
grand_entier* division(grand_entier* n1, grand_entier* dix);



char signe = '+';
void syntax_error(char wors [1000]);






/*

       L'IMPLEMENTATION DES METHODES DE LA STRUCTURE CELLULE

*/

// la methode pour creer une cellule
void push( cellule** head_ref, int new_data)
{
    // allocate cellule
    cellule* new_cel = new_cellule(new_data);

    // link the old list off the new cellule
    new_cel->suivant = (*head_ref);

    // move the head to point to the new cellule         23  // char = 32
    (*head_ref) = new_cel;
}

// creer une nouvelle cellule et alloue de la memoire
cellule* new_cellule(int data)
{
    cellule* new_ = ( cellule*)malloc(sizeof( cellule));
    if(new_ == NULL) longjmp(env,21); // si l'allocation a echouer et quitte le programme

    new_->chiffre = data; // on assigne le chiffre
    new_->suivant = NULL; // la prochaine cellule est NULl
    return new_; // on retourne la nouvelle cellule pour reaffecter le nombre entier.
}


/*
 * Cette methode affiche un grand entier sur la console
 */
void printList( cellule* cell)
{
    if(cell == NULL) return;

    cellule* cellu = reverse_iterative(cell);
    cellule* origin = cellu;
    // on retire s'il ya des 0 unitules
    //  cellule* final = (result);// on renverse pour retires les o facilement s'il en existe;
    while (cellu->chiffre == 0) {
        cellule* temp = cellu;
        cellu = cellu->suivant;
        free(temp);
    }
    while (cellu != NULL) {
        printf("%d", cellu->chiffre);
        if (cellu->suivant)
            printf("");
        cellu = cellu->suivant;
    }
    cell = reverse_iterative(origin);

}
// cette fonction prend en parametre deux grand entier et determine si les deux grand entiers sont
// egaux ou un plus grand que lautre ou inferieur a un autre
// et retourne 0, 1,-1 respectivement les cas precedents.
int compareTo(cellule* firs, cellule* secon){

    cellule* first = reverse_iterative(firs);
    cellule* second = reverse_iterative(secon);
    cellule* fst = first, *snd = second;
    int res  = 0;
    // tant qu'on a des nombres qui ont meme chiffres
    while (first && second )
    {
        if( first->chiffre > second->chiffre){
            res = 1;
            break;
        } else if (first->chiffre < second->chiffre){
            res = -1;
            break;
        } else res = 0;

        if(first)   first = first->suivant; // on passe au prochain chiffres
        if(second)  second = second->suivant;
    }

    // si une des liste de chiffres est vide, on evalue .
    if (first && !second) res =  1;
    if (second && !first) res=  -1;
    firs = reverse_iterative(fst);
    secon = reverse_iterative(snd);
    // sinon ces deux nombres sont egaux;

    return res;


}

// cette fonction prend un entier en parametre et retourne une liste chaine de longueur de cet entier
// et chaque position remoli de 0
cellule* make_empty_list(int size)
{
    struct cellule* head = NULL;
    while (size--)
        push(&head, 0);
    return head;
}

// cette methode prend en parametre une liste chaine representant un nombre
// et retourne la longueur de cette liste
int length(cellule* n){

    cellule* m = n;
    int size =0;
    while (m != NULL){
        m = m->suivant;
        size += 1;
    }
    return size;
}

cellule* reverse_iterative( cellule* head) {
    // no need to reverse if head is nullptr
    // or there is only 1 node.
    if (head == NULL ||
        head->suivant == NULL) {
        return head;
    }

    cellule* list_to_do = head->suivant;

    cellule* reversed_list = head;
    reversed_list->suivant = NULL;

    while (list_to_do != NULL) {
        cellule* temp = list_to_do;
        list_to_do = list_to_do->suivant;

        temp->suivant = reversed_list;
        reversed_list = temp;
    }

    return reversed_list;
}


/*
 * Cette fonction sumule la division entiere par 10 d'un grand_entier;
 */
cellule* divise(cellule* n1 ,int size){


    if(n1->chiffre > 0 && (size > 1) ){ // si on entier plus grand que 10 on decale d'un nombre
        cellule* tm = n1;
        n1 = n1->suivant;
        free(tm);
        return n1; // on retourne la nouvelle valeur
    } else{ // sinon c'est un entier < 10 donc cest 0;
        // printf("cas else ");
        cellule* p =  new_cellule(0);
        return p;
    }
}



cellule* soustraire(cellule* premier,cellule* second){

    cellule* copie_1 = premier,*copie_2 = second;
    int taille_p = length(premier); // la longueur des chiffres du premier nombtre
    int taille_s = length(second); // celle du deuxieme nombre

    int emprunt =0, difference;


    cellule* result = make_empty_list((taille_p > taille_s)? taille_p: taille_s);
    cellule *tmp_result = result;
    while (premier || second){
        // printf("debut du while");
        if(emprunt){ // si on a emprunter une dizaine et le reduit
            premier->chiffre -= 1;
            emprunt = 0;
        }
        if(premier  && second ){ // si les deux digits sont pas vides
            if(premier->chiffre < second->chiffre){ // et qu on le premier est < au deuxieme
                premier->chiffre += 10; // on empreinte une dizane au premier
                emprunt = 1; // pour la prochaine iteration on va retire une dizaine
            }
        }
        difference = ( premier? premier->chiffre:0) - (second?second->chiffre:0 ); // la difference
        tmp_result->chiffre = difference; // on ajouter au resultat
        tmp_result = tmp_result->suivant;
        if(result == NULL) result = tmp_result; // pour la premier fois on assigne a resultat le debut du tmp_result
        if(premier != NULL) premier = premier->suivant; // si premier n'est pas null on pas au chiffre suivant
        if(second != NULL) second= second->suivant; // meme chose pour second



    }
    //printf("le resultat "); printList(result); printf("\n");

    return (result); // on retourne le resultat

}

cellule* multiplie(cellule* premier, cellule* second) {



    cellule *copie_1;
    cellule *copie_2 = second;
    int a = length(premier);
    int b = length(second);
    cellule *result = make_empty_list(a + b + 1);// le resultat;
    cellule *result_tmp1 = result;
    cellule *result_tmp2;
    while (copie_2) {
        int retenue = 0;
        result_tmp2 = result_tmp1; // a chaque fois on recommence au prochain de la cellule
        copie_1 = premier;

        while (copie_1) {

            // on fait la multuplication du present chiffre de cellule 1 a cellulle en ajouter la retenue
            int mul = copie_1->chiffre * copie_2->chiffre + retenue;

            result_tmp2->chiffre += mul % 10;// on ajouter le modulo du resultat dans la cellule temp2

            retenue = mul / 10 + result_tmp2->chiffre / 10; // si on a une retenue?
            result_tmp2->chiffre = result_tmp2->chiffre % 10;

            copie_1 = copie_1->suivant; // on passe au suivant;
            result_tmp2 = result_tmp2->suivant;

        }

        if (retenue > 0) { // avons nous une retenue ?;
            result_tmp2->chiffre += retenue; // on ajoute la retenue
        }
        result_tmp1 = result_tmp1->suivant;
        copie_2 = copie_2->suivant;

    }

    return result;
}

// Le modulo 10 sur une strucutre cellule representant un grand entier
cellule* modulo_10(cellule* first ){

    int mod = first->chiffre;
    free_nombre(first); // on libere l'espace pour le nombre allouee
    first = new_cellule(mod); // on cree une nouvelle
    return first;

}

//// cette methode libere l'espace memoire allouer pour un nombre
void free_nombre(cellule *m) {
    cellule *p = m;
    cellule *next;

    while (p != NULL) {
        next = p->suivant;
        free(p);
        p = next;
    }
}

// FIN DE L'IMPLEMENTATIONS DE LA STRUCTURE CELLULE
/******************************************************************************************************/

/*
    IMPLEMENTATIONS DES METHODES DE LA STRUCTURE GRAND_ENTIER
    LES 4 OPERATIONS SUR LES GRANDS ENTIER +,- , %10, /10;
 */

// la methode pour lire le nombre entre a la console
grand_entier* completer_sequence(grand_entier* entier,int c){

    if(entier == NULL){ // si c'est la premiere fois on cree la structure grand_entier et on alloue de la memoire
        entier = malloc(sizeof(grand_entier));
        if(entier == NULL)  longjmp(env,21); // on arrete le programme lorsque l'allocation a echouer

        if ( c != 0) entier->chiffres = new_cellule(c); // si tout ce passe bien, on cree alors le nombre
        else entier->chiffres = NULL;
        entier->negatif = 0;
        // entier_tab[entier_count++] = entier;
        signe = '+';

    } else{  // si on a deja une sequence de chiffre
        cellule* p = entier->chiffres;
        cellule* q = new_cellule(c);
        q->suivant = p;
        entier->chiffres = q;
    }


    return entier;
}


// la methode qui sumule l'addition de deux ou plusieurs grand entier
grand_entier* addition(grand_entier* premier, grand_entier* second){



    int size_p = length(premier->chiffres); // la longueur du premier nombre en terme de chiffres
    int size_s = length(second->chiffres); // la longueur du second
    int sp = (size_p < size_s)? size_s: size_p;
    cellule* result = make_empty_list(sp +1); // le maximun du chiffre
    // pour le resultat de l'addition est la somme de longueur du premier et 2e plus 1

    int retenue = 0;// la retenue s'il en resulte
    int somme = 0;

    cellule* copie_1 = premier->chiffres, *copie_2 = second->chiffres;// copie des deux strucutre
    cellule* courant = result;

    while(copie_1 != NULL || copie_2 != NULL){
        // on fait l'addition de chaque digit des deux structures
        somme = retenue + (copie_1? copie_1->chiffre: 0) + (copie_2? copie_2->chiffre: 0);
        retenue = somme/10; // a t-on une retenue;

        courant->chiffre = somme % 10;
        courant = courant->suivant;
        if(copie_1 != NULL) copie_1 = copie_1->suivant; // si on est pas a la fin
        if(copie_2 != NULL) copie_2 = copie_2->suivant; // on continue au prochain chiffre
    }
    if(retenue > 0){

        courant->chiffre = retenue;
    }
    grand_entier * res = completer_sequence(NULL,0);
    res->chiffres = result;

    return res;
}

/*
    La soustraction sur les grands entiers
*/
grand_entier* soustraction(grand_entier* n1, grand_entier* n2){


    grand_entier* result  ; // le resultat de la soustraction
    if(n1 == n2){

        return completer_sequence(NULL,0);
    }

    int x  = compareTo(n1->chiffres,n2->chiffres); // on compare si le premier nombre > que le second
    cellule* p;
    if( x == -1 ){ // si le premier est < au second
        result = completer_sequence(NULL,0);
        result->negatif =1; // alors le resultat est negation
        p = soustraire(n2->chiffres,n1->chiffres); // puis on appelle la soustractions sur les cellules
        result->chiffres = p;
    } else
    {
        result = completer_sequence(NULL,0);
        p = soustraire(n1->chiffres,n2->chiffres);
        result->negatif = 0; // sinon c'est un nombre positif
        result->chiffres = p;
    }


    return result; // puis on retourne l'entier
}

/*
    La multiplication sur les grands entiers
*/

grand_entier* multiplication(grand_entier* n1 , grand_entier* n2){

    grand_entier* result = malloc(sizeof(grand_entier));
    // le cas special pour zero
    if(n1->chiffres == NULL || n2->chiffres == NULL){
        result->negatif =0;
        result->chiffres = NULL;
        return result;
    }
    // sinon on determine le resultat;
    if(n1->negatif || n2->negatif){ // determiner le signe du resultat
        if(n1->negatif && n2->negatif) result->negatif = 0;
        else result->negatif =1;
    }

    result->chiffres = multiplie(n1->chiffres,n2->chiffres);
    return result;
}


/*
    La division par 10 sur les grands entiers
*/


grand_entier* division(grand_entier* n1, grand_entier* n2){
    int size = length(n1->chiffres);
    grand_entier* ten = completer_sequence(NULL,1);
    ten = completer_sequence(ten,0);
    int x = comparer_entier(n2,ten) ;
    free_nombre(ten->chiffres);
    free(ten);
    printf("la valeur de x : %d", x);
    if(x==0){

        grand_entier* res = completer_sequence(NULL,0);
        if( size == 1) {
            res = completer_sequence(NULL,1);
            return res;
        }
        cellule* p = divise(n1->chiffres,size);
        res->chiffres = p;
        return res;
    } else{
        syntax_error("DivisionError: la division par 10 est permise.\n");
    }
}




grand_entier* modulo_entier(grand_entier* n1, grand_entier* n2){
    int size = length(n1->chiffres);

    cellule* ten = new_cellule(0);
    ten->suivant = new_cellule(1);
    int is_ten = compareTo(n2->chiffres,ten) == 0;
    if( (size > 1) && (is_ten) ){
        grand_entier* res = completer_sequence(NULL,0);
        cellule* p =  modulo_10(n1->chiffres);
        res->chiffres = p;
        free_nombre(ten);
        return res;
    } else if(is_ten){
        return  n1;
    } else{
        syntax_error("ModuloError: juste le modulo 10 existe\n ");
    }

}

// IMPLEMENTATIONS DES METHODES DE LA STRUCTURE CELLULE
/*********************************************************************************
 *
 * @param data
 * @return cree une nouvelle structure ,alloue la memoire et retouerne la structure
 */


void print_entier(grand_entier* p){
    if(p == NULL)
    {
        return;
    } else if ( p->chiffres == NULL) {
        printf("0");
        return;
    } else


    if(p->negatif == 1){
        printf("-");
    }

    printList(p->chiffres);
}

int comparer_entier(grand_entier* n1, grand_entier* n2){

    int res = 0;
    res = compareTo(n1->chiffres,n2->chiffres);
    if( n1->negatif && n2->negatif){
        if(res == 1) res = -1;
        else if(res == -1) res = 1;
    } else if(n1->negatif){
        res = -1;
    } else {
        res = 1;
    }


    return res;
}

int zero_compare(grand_entier* n,char ts){

    cellule* cellu = reverse_iterative(n->chiffres);
    cellule* origin = cellu;
    int res;

    switch (ts){

        case '=' :{
            if(n->negatif) res =0;
            else if(cellu == NULL) res = 1;
            else res =0;
            break;
        }
        case '<':
            if(n->negatif) res = 1;
            else res = 0;
            break;
        case '>':{
            if(n->negatif) res = 0;
            if(n->chiffres->chiffre > 0) res =1;
            else res =0;
            break;
        }
    }
    n->chiffres = reverse_iterative(origin);
    return res;
}

// les fonctions de liberation de memoires

void free_entier(grand_entier* tab[]);





/*************************************************************************************************************/

struct addresse{

    char debut;
    char fin;
    struct addresse* next;
};

typedef struct  addresse address;

address* new_instruction(char  deb, char fin){

    address* n = malloc(sizeof(address));
    if(n == NULL) longjmp(env,21); // si malloc retourne null on quitte le programme;
    n->debut = deb;
    n->fin = fin;
    n->next = NULL;
    return n;
}
address* add_new_adress(address* a , char deb,char fin){

    if(a == NULL){
        a = new_instruction(deb,fin);
    } else{
        a->next = new_instruction(deb,fin);
    }
    return a;
}
address* do_while_data = NULL ;


/****************************************************************************************************************/

grand_entier* current ;

void memory_error();

/*---------------------------------------------------------------------------*/

/* Analyseur lexical. */

enum { DO_SYM, ELSE_SYM, IF_SYM, WHILE_SYM, LBRA, RBRA, LPAR,
    RPAR, PLUS, MINUS, LESS, SEMI, EQUAL, EQUAL_TEST,INT, ID, EOI,PRINT_SYM,MULT ,COMMA,GREATER,GREAT_EQ,
    LESS_EQUAL,NOT_EQ,DIVB,MODULO,ADRESSE_SYM,BREAK_SYM,CONTI_SYM,GOTO_SYM};

char *words[] = { "do", "else", "if", "while","print", NULL };

int ch = ' ';
int sym;
int int_val;
grand_entier* current ;
char id_name[100];

void syntax_error(char word [110000]) {
    printf("%s",word);
    longjmp(env,32);

}

void next_ch() { ch = getchar(); }

void next_sym()
{
    while (ch == ' ' || ch == '\n') next_ch();
    switch (ch)
    { case '{': sym = LBRA;  next_ch(); break;
        case '}': sym = RBRA;  next_ch(); break;
        case '(': sym = LPAR;  next_ch(); break;
        case ')': sym = RPAR;  next_ch(); break;
        case '+': sym = PLUS;  next_ch(); break;
        case '-': sym = MINUS; next_ch(); break;
        case '<': {
            next_ch();
            if(ch != '=') { sym = LESS;}/* <= ? si oui on a le a <= b sinon a < b */
            else { sym = LESS_EQUAL; next_ch();}/* sinon sym recoit LESS_EQ*/
            break;
        }
        case ';': sym = SEMI;  next_ch(); break;
        case '=': {
            next_ch();
            if (ch != '=') sym = EQUAL;
            else {
                sym = EQUAL_TEST;
                next_ch();
            }
            break;
        }
        case '*': sym = MULT; next_ch(); break;
        case '\"': sym = COMMA ; next_ch();break;
        case '>' : {
            next_ch();
            if(ch != '=') { sym = GREATER;}/* cas ou on a  juste ce signe < */
            else { sym = GREAT_EQ; next_ch();}/* sinon sym recoit <=  */
            break;
        }
        case '!': {
            next_ch();
            if(ch == '=') sym = NOT_EQ; next_ch();
            break;
        }
        case '/': sym = DIVB; next_ch(); break;
        case '%': sym = MODULO; next_ch(); break;
        case EOF: sym = EOI;   next_ch(); break;
        case '^': sym =  EOI; next_ch();
            break;
        default:
            if (ch >= '0' && ch <= '9')
            {
                int_val = 0; /* overflow? */

                while (ch >= '0' && ch <= '9')
                {
                    int_val =  ch - '0';
                    current = completer_sequence(current,int_val);
                    next_ch();
                }

                sym = INT;
            }
            else if (ch >= 'a' && ch <= 'z')
            {
                int i = 0; /* overflow? */

                while ((ch >= 'a' && ch <= 'z') || ch == '_')
                {
                    id_name[i++] = ch;
                    next_ch();
                }

                id_name[i] = '\0';
                sym = 0;

                while (words[sym]!=NULL && strcmp(words[sym], id_name)!=0)
                    sym++;

                if (words[sym] == NULL)
                {
                    if (id_name[1] == '\0') sym = ID; else syntax_error("ID ERROR ");
                } else {
                    if(strcmp("while", id_name) == 0){// le while
                        sym = WHILE_SYM;

                    } else if(strcmp("do",id_name) == 0){ // le do while
                        sym = DO_SYM;
                    } else if (strcmp("if",id_name) == 0) {
                        printf("dans IF ");
                        sym = IF_SYM; // le if
                    }
                    else
                        sym = PRINT_SYM; // le print

                }
            }
    }
}

/*---------------------------------------------------------------------------*/

/* Analyseur syntaxique. */
int valeur[26];
enum { VAR, CST, ADD, SUB, LT, ASSIGN,
    IF1, IF2, WHILE, DO, EMPTY, SEQ, EXPR, PROG ,LT_EQ,GT_EQ,EQ,GT, MUL,DIV,PRINT,MOD,NOT
};

struct node
{
    int kind;
    struct node *o1;
    struct node *o2;
    struct node *o3;
    grand_entier* val;
};

typedef struct node node;

node *new_node(int k)
{
    node *x = malloc(sizeof(node));
    if( x == NULL ) syntax_error("MemoryError: alloction memoire echouee");
    x->kind = k;
    x->o1 = NULL;
    x->o2 = NULL;
    x->o3 = NULL;
    x->val = NULL;
    return x;
}

node *paren_expr(); /* forward declaration */

node *term() /* <term> ::= <id> | <int> | <paren_expr> */
{
    node *x;

    if (sym == ID)           /* <term> ::= <id> */
    {
        x = new_node(VAR);
        x->val = completer_sequence(NULL, 0);
        x->val->negatif = id_name[0]-'a';
        next_sym();
    }
    else if (sym == INT)     /* <term> ::= <int> */
    {
        x = new_node(CST);
        x->val = current;
        current = NULL;
        next_sym();
    }
    else                     /* <term> ::= <paren_expr> */
        x = paren_expr();

    return x;
}

node *sum() /* <sum> ::= <term>|<sum>"+"<term>|<sum>"-"<term> */
{
    node *x = term();
    while ((sym == PLUS) || (sym == MINUS) || (sym==MULT) || (sym==MODULO) || (sym== DIVB))
    {
        node *t = x;
        switch (sym){ // on identifie l'operation pour faire le calcul qui correspond;
            case PLUS: x = new_node(ADD); break;
            case MINUS: x = new_node(SUB); break;
            case MULT: x= new_node(MUL); break;
            case DIVB: x= new_node(DIV); break;
            case MODULO: x = new_node(MOD); break;
        }
        next_sym();
        x->o1 = t;
        x->o2 = term();
        // si on a une multiplication ou une division alors on fait d'abord les priorites avant + et -
        while(sym == MULT || sym == DIVB){
            node* m = x->o2;
            switch (sym){ // on identifie l'operation pour faire le calcul qui correspond;
                case MULT: x->o2 = new_node(MULT); break;
                case DIVB: x->o2 = new_node(DIV); break;
            }

            next_sym();
            x->o2->o1 = m;
            x->o2->o2 = term();
        }
//        printf("nombre 1: ");
//        printList(t->val->chiffres);
//        printf("nombre 2: ");
//        printList(x->o2->val->chiffres);

        // printf ("[x1: %d , x2 : %d]",t->val,x->o2->val);
        // printf ("[x1: %d , x2 : %d]",t->val,x->o2->val);

    }
    return x;
}

node *test() /* <test> ::= <sum> | <sum> "<" <sum> */
{
    node *x = sum();
    node *t;
    int is_test = 0;
    switch (sym) {
        case LESS :
            t = x;
            x = new_node(LT);
            is_test++;
            break; /*<sum> "<"  <sum>*/
        case LESS_EQUAL:
            t = x, x = new_node(LT_EQ);
            is_test++;
            break; /*<sum> "<=" <sum>*/
        case GREATER :
            t = x, x = new_node(GT);
            is_test++;
            break; /*<sum> ">" <sum>*/
        case GREAT_EQ:
            t = x, x = new_node(GT_EQ);
            is_test++;
            break; /*<sum> ">=" <sum>*/
        case NOT_EQ:
            t = x, x = new_node(NOT);
            is_test++;
            break; /*<sum> "!=" <sum>*/
        case EQUAL_TEST :
            t = x, x = new_node(EQ);
            is_test++;
            break; /*<sum> "==" <sum>*/

    }
    if (is_test) {

        next_sym();
        x->o1 = t;
        x->o2 = sum();

    }
    return x;
}

node *expr() /* <expr> ::= <test> | <id> "=" <expr> */
{
    node *x;

    if (sym != ID) return test();

    x = test();

    if (sym == EQUAL)
    {
        node *t = x;
        x = new_node(ASSIGN);
        next_sym();
        x->o1 = t;
        x->o2 = expr();
    }

    return x;
}

node *paren_expr() /* <paren_expr> ::= "(" <expr> ")" */
{
    node *x;

    if (sym == LPAR) next_sym(); else syntax_error("SYM != ( , ERROR SYNTAX");

    x = expr();

    if (sym == RPAR) next_sym(); else syntax_error(" SYM != )  SYNTAX ERROR");

    return x;
}

node *statement()
{
    node *x;

    if (sym == IF_SYM)       /* "if" <paren_expr> <stat> */
    {
        x = new_node(IF1);
        next_sym();
        x->o1 = paren_expr();
        x->o2 = statement();
        if (sym == ELSE_SYM) /* ... "else" <stat> */
        { x->kind = IF2;
            next_sym();
            x->o3 = statement();
        }
    }
    else if (sym == WHILE_SYM) /* "while" <paren_expr> <stat> */
    {
        x = new_node(WHILE);
        next_sym();
        x->o1 = paren_expr();
        x->o2 = statement();
    } else if(sym == PRINT_SYM){

        x = new_node(PRINT);

        next_sym();

        x->o1 = paren_expr();

        if (sym == SEMI) next_sym(); else syntax_error("PRINT ERROR 878");
    }
    else if (sym == DO_SYM)  /* "do" <stat> "while" <paren_expr> ";" */
    {
        x = new_node(DO);
        next_sym();
        x->o1 = statement();
        if (sym == WHILE_SYM) next_sym(); else syntax_error("DO WHILE SANS WHILE ERROR ");
        x->o2 = paren_expr();
        if (sym == SEMI) next_sym(); else syntax_error("DO WHILE ERRO SYM != ;");
    }
    else if (sym == SEMI)    /* ";" */
    {
        x = new_node(EMPTY);
        next_sym();
    }
    else if (sym == LBRA)    /* "{" { <stat> } "}" */
    {
        x = new_node(EMPTY);
        next_sym();
        while (sym != RBRA)
        {
            node *t = x;
            x = new_node(SEQ);
            x->o1 = t;
            x->o2 = statement();
        }
        next_sym();
    }
    else                     /* <expr> ";" */
    {
        x = new_node(EXPR);
        x->o1 = expr();
        if (sym == SEMI) next_sym(); else syntax_error("MANQUE ; FIN ");
    }

    return x;
}

node *program()  /* <program> ::= <stat> */
{
    node *x = new_node(PROG);
    next_sym();
    x->o1 = statement();
    //if (sym != EOI) syntax_error();
    return x;
}

/*---------------------------------------------------------------------------*/

/* Generateur de code. */

enum { ILOAD, ISTORE, BIPUSH, DUP, POP, IADD, ISUB,
    GOTO, IFEQ, IFNE, IFLT, RETURN ,IPRINT,IMULT,IDIV,IMOD};

typedef signed char code;

code object[1000], *here = object;
grand_entier* tableau[1000];
int pos = 0;
grand_entier** stack_entier = tableau;
int  ajouter_nombre(grand_entier* m){

   // printList(m->chiffres);// printf("\n");
    //printf("ajout \n");
    tableau[pos++] = m;
    return  pos -1;

}

int compare_to_zero(grand_entier* n,char c){


    int res = 0;
    res= zero_compare(n,c);
    return res;
    ;
}
void gen(code c) { *here++ = c; } /* overflow? */
//
#ifdef SHOW_CODE
#define g(c) do { printf(" %d",c); gen(c); } while (0)
#define gi(c) do { printf("\n%s ", #c); gen(c); } while (0)
#else
#define g(c) gen(c)
#define gi(c) gen(c)
#endif

void fix(code *src, code *dst) { *src = dst-src; } /* overflow? */

void ajout_zeros();
void c(node *x){
    switch (x->kind)
    { case VAR   : gi(ILOAD); g(x->val->negatif); break;

        case CST   : gi(BIPUSH); g(ajouter_nombre(x->val));  break;

        case ADD   : c(x->o1); c(x->o2); gi(IADD); break; // <sum> + <sum>

        case SUB   : c(x->o1); c(x->o2); gi(ISUB); break; // <sum> - <sum>
        case MUL:  c(x->o1); c(x->o2); gi(IMULT); break; //<sum> * <sum>
        case DIV:  c(x->o1); c(x->o2); gi(IDIV); break; // <sum> / 10;
        case MOD:  c(x->o1); c(x->o2); gi(IMOD); break; // <sum> % 10

        case LT    :
            gi(BIPUSH); g(1); //ajouter_nombre(one);
            c(x->o1);
            c(x->o2);
            gi(ISUB);
            gi(IFLT); g(4);//ajouter_nombre(katre)
            gi(POP);
            gi(BIPUSH); g(0); // ajout_zeros();

            break;
        case GT_EQ:{
            c(x->o2);
            c(x->o1);
            gi(ISUB);
            gi(DUP);
            gi(IFEQ); g(8);
            gi(IFLT); g(7);
            gi(POP);
            gi(BIPUSH); g(0);
            gi(GOTO); g(2);
            gi(POP); break;
        }
        case ASSIGN: c(x->o2);
            gi(DUP);
            gi(ISTORE); g(x->o1->val->negatif); break;

        case IF1   : { code *p1;
            c(x->o1);
            gi(IFEQ); p1 = here++;
            c(x->o2); fix(p1,here); break;
        }
        case NOT:{
            gi(BIPUSH); g(0);
            c(x->o1);
            c(x->o2);
            gi(ISUB);
            //gi(IPRINT);
            gi(IFEQ); g(4);
            gi(POP);
            gi(BIPUSH); g(1); break;// nouveau pour le !=

        }

        case EQ:{
            gi(BIPUSH); g(1);
            c(x->o1);
            c(x->o2);
            gi(ISUB);
            gi(IFEQ); g(4);
            gi(POP);
            gi(BIPUSH); g(1); break;//
        }

        case IF2   : { code *p1, *p2;
            c(x->o1);
            gi(IFEQ); p1 = here++;
            c(x->o2);
            gi(GOTO); p2 = here++; fix(p1,here);
            c(x->o3); fix(p2,here); break;
        }



        case WHILE : { code *p1 = here, *p2;
            // printf("**** p1 : [ %d ] *** ",*p1);
            c(x->o1);
            gi(IFEQ); p2 = here++; // printf("**** p2 : [ %d ] *** ",*p2);
            c(x->o2);
            gi(GOTO); fix(here++,p1); fix(p2,here); break;
        }

        case DO    : { code *p1 = here; c(x->o1);
            c(x->o2);
            gi(IFNE); fix(here++,p1); break;
        }

        case EMPTY : break;

        case SEQ   : c(x->o1);
            c(x->o2); break;

        case EXPR  : c(x->o1);
            gi(POP); break;


        case PROG  : c(x->o1);
            gi(RETURN); break;
        case PRINT : {
            c(x->o1);

            gi(IPRINT);
            ;break;
        }
    }

}

/*---------------------------------------------------------------------------*/

/* Machine virtuelle. */

grand_entier* globals[26];

void run()
{
    grand_entier* stack[1000], **sp = stack; /* overflow? */
    code *pc = object;
    int pp = 0;
    for (;;){
        switch (*pc++)
        {
            case ILOAD : *sp++ = globals[*pc++];            break;
            case ISTORE: globals[*pc++] = *--sp;        break;
            case BIPUSH: *sp++ = tableau[*pc++];        break;
            case DUP   : sp++; sp[-1] = sp[-2];            break;
            case POP   : --sp ;                            break;
            case IADD  : sp[-2] = addition(sp[-2] , sp[-1]);  --sp;   break;
            case ISUB  :  sp[-2] = soustraction(sp[-2] , sp[-1]);  --sp;break;
            case GOTO  : pc += *pc;                     break;
            case IFEQ  : {
                int  x = compare_to_zero((*--sp),'=');
                if (x ) pc += *pc ;  else pc ++;
                break;
            }
            case IFNE  :{
                int x  = compare_to_zero((*--sp),'!');

                if(x == 0 ) pc += *pc ; else pc++;
                break;
            }
            case IFLT  : {

                int x = compare_to_zero (*sp,'<');
                if(x) pc += *pc; else pc++;

                break;
            }
            case IPRINT: print_entier((*--sp)) ; printf("\n"); break;

            case IMULT:  sp[-2] = multiplication(sp[-2] , sp[-1]); --sp;     break;
            case IDIV:  sp[-2] = division(sp[-2] , sp[-1]); --sp;     break;
            case IMOD:  sp[-2] = modulo_entier(sp[-2] , sp[-1]); --sp;     break;

            case RETURN: {


                return;
            }
        }
    }


}
void free_memoire_node(node *root){

    if(root == NULL)
        return;

    if(root->o1 != NULL){
        free_memoire_node(root->o1);
        root->o1 = NULL;
    }
    if(root->o2 != NULL){
        free_memoire_node(root->o2);
        root->o2 = NULL;
    }  if(root->o3 != NULL){

        free_memoire_node(root->o3);
    }

    //printf("deleting kind : %d \n", #root->kind);

    free(root);


}

void free_entier(grand_entier** n){

    for ( int r= 0; r < pos; r++){
        free_nombre((*n)->chiffres);
        free((*n));
        n++;
    }
    printf("fin de liberation entier");
}
/*---------------------------------------------------------------------------*/

/* Programme principal. */

int main()
{
    int i;
    node* n;
    c(program());
//
//    if(!setjmp(env)){
//        n = program();
//        c(n);
//        //printf("retours sans probleme");
//    } else {
//        // free_entier(tableau);
//        //free_memoire_node(n);
//        exit(-2);
//    }


#ifdef SHOW_CODE
    printf("\n");
#endif



    run();
    // printf("liberation de la memoire" );
    //  free_entier(tableau);
    //free_memoire_node(n);
    // printf("la memoire liberee \n");


    return 0;
}

/*---------------------------------------------------------------------------*/
